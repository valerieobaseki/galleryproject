import * as React from "react";
import { Routes, Route} from "react-router-dom";
import "./App.css";
import "./w3.css"
import Header from "./components/Header";
import Gallery from "./components/Gallery";
import Footer from "./components/Footer";
import Home from "./pages/Home";


function App() {
  return (
    <>
    <div className={"w3-content max-width"} >

      <Header />
      <Gallery />
      <Footer />
      <Routes>
        <Route path="/" element={<Home />} />
       
      </Routes>
      </div>
    </>
  );
}

export default App;
