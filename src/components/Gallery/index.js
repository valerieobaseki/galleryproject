import React from 'react'

const Gallery = () => {
  return (
<div className={"w3-row-padding w3-grayscale"} style={{marginbottom:"128px"}}>
  <div className={"w3-half"}>
    <img src="https://www.w3schools.com/w3images/wedding.jpg" alt="" style={{width:"100%"}}/>
    <img src="https://www.w3schools.com/w3images/rocks.jpg" alt="" style={{width:"100%"}}/>
    <img src="https://www.w3schools.com/w3images/falls2.jpg" alt="" style={{width:"100%"}}/>
    <img src="https://www.w3schools.com/w3images/paris.jpg" alt=""style={{width:"100%"}}/>
    <img src="https://www.w3schools.com/w3images/nature.jpg" alt="" style={{width:"100%"}}/>
    <img src="https://www.w3schools.com/w3images/mist.jpg" alt="" style={{width:"100%"}}/>
    <img src="https://www.w3schools.com/w3images/paris.jpg" alt="" style={{width:"100%"}}/>
  </div>

  <div className={"w3-half"}>
    <img src="https://www.w3schools.com/w3images/underwater.jpg" alt="" style={{width:"100%"}}/>
    <img src="https://www.w3schools.com/w3images/ocean.jpg" alt="" style={{width:"100%"}}/>
    <img src="https://www.w3schools.com/w3images/wedding.jpg" alt="" style={{width:"100%"}}/>
    <img src="https://www.w3schools.com/w3images/mountainskies.jpg" alt="" style={{width:"100%"}}/>
    <img src="https://www.w3schools.com/w3images/rocks.jpg" alt="" style={{width:"100%"}}/>
    <img src="https://www.w3schools.com/w3images/underwater.jpg" alt="" style={{width:"100%"}}/>
  </div>
</div>
  );
    
}

export default Gallery