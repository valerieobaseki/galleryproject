import React from 'react'

const Header = () => {
  return (
    <header className={'w3-panel w3-center w3-opacity" style="padding:128px 16px'}>
          <h1 className={'w3-xlarge'}>PHOTOGRAPHER</h1>
          <h1>John Doe</h1>

          <div className={'w3-padding-32'}>
              <div className={'w3-bar w3-border'}>
                  <a href="#" className={'w3-bar-item w3-button'}>Home</a>
                  <a href="#" className={'w3-bar-item w3-button w3-light-grey'}>Portfolio</a>
                  <a href="#" className={'w3-bar-item w3-button'}>Contact</a>
                  <a href="#" className={'w3-bar-item w3-button w3-hide-small'}>Weddings</a>
              </div>
          </div>
      </header>
  );
    
}

export default Header